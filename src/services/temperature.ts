import http from "./http"

type ReturnData = {
    celsius: number
    fahrenheit: number
  }
async function convert(celsius: number): Promise<number> {
    console.log("Service: call convert")
  console.log(`/temperature/convert/${celsius}`)
  // const res = await axios.get(`http://localhost:3000/temperature/convert/${celsius.value}`)
  const res = await http.post(`/temperature/convert`, { 
    celsius: celsius 
})
  const convertResult = res.data as ReturnData
  console.log("Service: Finish call convert")
  return convertResult.fahrenheit
}

export default { convert}