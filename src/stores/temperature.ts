import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperetureService from '@/services/temperature'

export const userTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()
 
  async function callConvert() {
    console.log('Store: call Convert')
    loadingStore.doLoad()
   try{
     
      result.value = await temperetureService.convert(celsius.value)
   }catch(e){
    console.log(e)
   }
    loadingStore.finish()
    console.log('Store: Finish call Convert')
  }
  return {valid, result, celsius, callConvert  }
})
